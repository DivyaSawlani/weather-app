//http://api.openweathermap.org/data/2.5/forecast?q=mumbai&units=metric&APPID=3c95b263c8cc5189ce1f538fe1abf9d0

const key = "3c95b263c8cc5189ce1f538fe1abf9d0";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
   // console.log(response);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error("Error status: "+response.status);
    }
}

//getForecast()
//    .then(data=>console.log(data))
//    .catch(err=>console.log(err));